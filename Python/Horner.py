"""p est une fonction polynôme, exprimée comme liste de ses coefficients.

Le premier est celui de degré n, non nul, puis les suivants sont tous
indiqués, quitte à ce qu'ils soient nuls, dans l'ordre des puissances
décroissantes jusqu'au coefficient constant.

Par exemple p(x) = 7x³ - 2x² + 5 se traduit par [7, -2, 0, 5].
"""

# TODO: ajouter l'affichage "version Hörner", voire le nombre d'opérations

def terme(ak, k, n):
    """Renvoie la chaîne à afficher pour "ak x^k", quand d° == n
    """
    # TODO: Améliorer, pour +0 ou -1 en tête...
    
    # 1°) Signe et coefficient
    txt = "" if k == n else " "
    if ak > 0:
        if k == n:
            txt += str(ak) if ak != 1 else ""
        else:
            txt += "+ {}".format(ak) if ak != 1 else " + "
    elif ak < 0:
        # On évite que le '-' soit collé au nombre
        txt = "- {}".format(-ak) if ak != -1 else "- "
    else:  # cas du terme nul
        txt  += "+ 0" if k == 0 else ""
    # Cas du +1 ou -1 comme coeff. du terme constant, donc à afficher
    if abs(ak) == 1 and k == 0:
        txt += "1"
        
    # 2°) x ou x^n, si besoin    
    if ak != 0 and k > 0:
        txt += "x"
        if k > 1: txt += "^{}".format(k)

    return txt


def horner(p, x, txt=False):
    """Applique la méthode de Horner pour p(x).
    
    Renvoie (y, devel) où 
     . y vaut p(x)
     . devel est la chaîne "forme développée de p" (ou None si txt==False)
    """
    devel = None
    y = p[0]
    assert y != 0 or len(p) == 1
    n = len(p) - 1
    if txt: devel = terme(y, n, n)

    k = n
    for coeff in p[1:]:
        k -= 1
        y = y*x + coeff

        if txt:
            devel += terme(coeff, k, n)
    #if afficher: print(devel, " =>  p({}) = {}".format(x, y))
    
    return y, devel
    
def image(p, x):
    """Calcule p(x), avec la méthode de Hörner.
    """
    return horner(p, x)[0]

tab = [ ([10, 1, 0, 5], 2), 
        ([10, 1, 0, 5], -2),
        ([+9, -5, -7, 1], 2),
        ([0], 2),
        ([-1], 2),
        ([1], 2),
        ([7], 2),
        ]
for p, x in tab:
    y, txt = horner(p, x, True)
    print("p(x) = {} \t=> \tp({}) = {}".format(txt, x, y))
