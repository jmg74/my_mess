import turtle as TT
# Documentation, notamment sur
#   https://fr.wikibooks.org/wiki/Programmation_Python/Turtle
#   https://docs.python.org/3.6/library/turtle.html

TT.speed("fastest")  # "slowest", "slow", "normal", "fast", "fastest" (=0), ou 1 à 10 
TT.color("blue", "orange")     # couleur de tracé, puis du fond pour «colorier»)

taille = 100           # longueur du segment tracé

print("largeur de fenêtre :", TT.window_width())	# Affiché dans la console
print("hauteur de fenêtre :", TT.window_height())



def carre(taille, n):
    if n <= 0: return
    for i in range(4):
        TT.forward(taille)
        TT.right(90)
    carre(taille * 0.7, n - 1)

carre(100, 10)

TT.hideturtle()
TT.exitonclick()
