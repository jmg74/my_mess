def factorielle(n):
    def factorielle_aux(accu, n):
        if n == 0:
            return accu
        else:
            return factorielle_aux(n * accu, n - 1)
    return factorielle_aux(1, n)
