from random import randint

NB_LANCERS = 1000000

##nb9 = 0
##nb10 = 0
##
##for i in range(NB_LANCERS):
##    d1, d2, d3 = randint(1,6), randint(1,6), randint(1,6)
##    s = d1 + d2 + d3
##    if s == 9:
##        nb9 += 1
##    elif s == 10:
##        nb10 += 1
##
##print("fréq. (9) =", nb9 / NB_LANCERS)
##print("fréq. (10) =", nb10 / NB_LANCERS)

lancer = [randint(1,6)+randint(1,6)+randint(1,6) for i in range(NB_LANCERS)]
nb9 = len([1 for res in lancer if res == 9])
nb10 = len([1 for res in lancer if res == 10])

print("fréq. (9) =", nb9 / NB_LANCERS)
print("fréq. (10) =", nb10 / NB_LANCERS)

